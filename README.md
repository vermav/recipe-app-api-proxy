# recipe-app-api-proxy

NGINX Recipe App Proxy

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname to forward the request to (default: `app`)
* `APP-PORT` - Default port on app (default: `9000`)